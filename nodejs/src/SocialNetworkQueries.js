class SocialNetworkQueries {
    constructor({ fetchCurrentUser }) {
        this.fetchCurrentUser = fetchCurrentUser;
    }

    async findPotentialLikes({ minimalScore } = {}) {
        let currentUser;
        const books = [];
        const noLikes = [];

        try {
            currentUser = await this.fetchCurrentUser();
        } catch (error) {
            return Promise.resolve({ books });
        }

        if (!currentUser.friends || currentUser.friends.length == 0) {
            return Promise.resolve({ books });
        }

        for (let i = 0; i < currentUser.likes.books.length; i++) {
            const book = currentUser.likes.books[i];
            noLikes.push(book);
        }

        const aggregateBooks = currentUser.friends.reduce((total, current) => {
            current.likes.books.forEach(book => {
                if (!noLikes.includes(book)) {
                    total[book] = total[book] ? total[book] + 1 : 1;
                }
            })
            return total
        }, {}) || {}

        Object.keys(aggregateBooks)
            .sort((a, b) => (aggregateBooks[b] - aggregateBooks[a] || a.localeCompare(b)))
            .forEach((key) => {
                if (aggregateBooks[key] / currentUser.friends.length >= minimalScore) {
                    books.push(key);
                }
            })

        return Promise.resolve({ books });
    }
}

export { SocialNetworkQueries };
