import React, { useReducer, useEffect } from 'react';
import { Movie, MoviesAction } from 'types';
import { getMovies } from 'api/movies';

interface MoviesState {
  movies: Movie[];
  initialized: boolean;
}

export function useMoviesReducer(): [MoviesState, React.Dispatch<MoviesAction>] {
  const movieReducer = (state: MoviesState, action: MoviesAction): MoviesState => {
    switch (action.type) {
      case 'fetch':
        return { initialized: true, movies: action.payload.data };

      case 'add':
        const movie: Movie = {
          ...action.payload.movie,
          id: Date.now().toString(),
          ratings: [],
        };
        return { ...state, movies: [...state.movies, movie] };

      case 'delete':
        return { ...state, movies: state.movies.filter((movie) => movie.id !== action.payload.movieId) };

      case 'rate':
        return {
          ...state,
          movies: state.movies.map((movie: Movie) => {
            if (movie.id !== action.payload.movieId) {
              return movie;
            }
            movie.ratings.push(action.payload.rating);
            return movie;
          }),
        };

      default:
        return state;
    }
  };

  const [state, dispatch] = useReducer(movieReducer, {
    movies: [],
    initialized: false,
  });

  useEffect(() => {
    getMovies().then((res) => {
      dispatch({
        type: 'fetch',
        payload: {
          data: res,
        },
      });
    });
  }, []);

  return [state, dispatch];
}
