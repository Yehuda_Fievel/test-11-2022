import { useState } from 'react';
import { InputField, Button } from 'shared/components';
// import { MoviesAction } from 'types';

interface AddMovieFormProps {
  onSubmit: (data: Record<'imageUrl' | 'title' | 'subtitle' | 'description', string>) => void;
  onCancel: () => void;
}

export function AddMovieForm({ onSubmit, onCancel }: AddMovieFormProps) {
  const [movie, setMovie] = useState<any>({});

  const setter = (target: any) => {
    const { name, value } = target;
    setMovie({
      ...movie,
      [name.toLowerCase()]: value,
    });
  };

  const onClick = () => {
    setMovie({});
    onSubmit(movie);
  };

  return (
    <form className='p-4 '>
      <InputField name='Url' setter={setter} />
      <InputField name='Title' setter={setter} />
      <InputField name='Subtitle' setter={setter} />
      <InputField name='Description' setter={setter} />
      <div className='text-center'>
        <Button onClick={onClick}>Submit</Button>
        <Button onClick={onCancel}>Cancel</Button>
      </div>
    </form>
  );
}
