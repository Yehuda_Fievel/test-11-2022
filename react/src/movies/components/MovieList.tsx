import { useState } from 'react';
import { MovieCard } from './MovieCard';
import { AddMovieButton } from './AddMovieButton';
import { AddMovieForm } from './AddMovieForm';
import { Card } from 'shared/components';
import { useMovies } from './MovieProvider';

// type NewMovieMode = 'BUTTON' | 'FORM';

export const MovieList = () => {
  const { movies, moviesDispatch } = useMovies();
  const [displayOptionType, setDisplayOptionType] = useState('button');

  const onClickAddMovieButton = () => {
    setDisplayOptionType('form');
  };

  const onSubmit = (data: any) => {
    moviesDispatch({ type: 'add', payload: { movie: data } });
    setDisplayOptionType('button');
  };

  const onCancel = () => {
    setDisplayOptionType('button');
  };

  return (
    <div className='card-deck'>
      {movies.map((movie) => (
        <Card key={movie.id}>
          <MovieCard key={movie.id} movie={movie} />
        </Card>
      ))}
      <Card>
        {displayOptionType === 'button' ? (
          <AddMovieButton onClick={onClickAddMovieButton} />
        ) : (
          <AddMovieForm onSubmit={onSubmit} onCancel={onCancel} />
        )}
      </Card>
    </div>
  );
};
